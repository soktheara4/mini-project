@extends('layouts.master')
@section('title','Add Person')
@section('content')
        
<div class="row">
    <div class="col-md-12">
        <form role="form" method="POST" action="/add" enctype="multipart/form-data"> 
            @csrf
            <div class="form-group">
                 
                <label>
                    @lang("lang.name")
                </label>
            <input type="text" class="form-control" name="name" value="{{old('name')}}"/>
                @error('name')
                    <span style="color:red">@lang("lang.null")</span>
                @enderror
            </div>
            <div class="form-group">
                 
                <label >
                    @lang("lang.email")
                </label>
                <input type="mail" class="form-control" name="email" value="{{old('email')}}"/>
                @error('email')
                <span style="color:red">@lang("lang.null")</span>
                @enderror
            </div>
            <div class="form-group">
                 @lang("lang.phone")
                </label>
                <input type="text" class="form-control" name="phone_number" value="{{old('phone_number')}}"/>
                @error('phone_number')
                <span style="color:red">@lang("lang.null")</span>
                @enderror
            </div>


            <div class="form-group">
                 
                <label >
                   @lang("lang.profile")
                </label>
                <input type="file" class="form-control-file" name="avator" />
                
            </div>
            
            <button type="submit" class="btn btn-primary">
                @lang("lang.add")
            </button>
        </form>
    </div>
</div>
@endsection