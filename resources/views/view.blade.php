@extends('layouts.master')
@section('title','View Person')
@section('content')
        
<div class="row">
    <div class="col-md-12">
        <form role="form" method="POST" action="/add" enctype="multipart/form-data"> 
            @csrf
            <div class="form-group">
                 
                <label>@lang("lang.name"):</label>
            <input type="text" class="form-control" name="name" value="{{$person->name}}" />
            </div>
            <div class="form-group"><label >@lang("lang.email"):</label>
                <input type="mail" class="form-control" name="email" value="{{$person->email}}" />
            </div>
            <div class="form-group">@lang("lang.phone"):</label>
            <input type="text" class="form-control" name="phone" value="{{$person->phone_number}}"/>
            </div>
            <div class="form-group">
                <label >@lang("lang.profile")</label>
                <img src="/storage{{$person->profile}}" alt="Avatar" class="rounded-circle" style="with:200px;height:200px;" > 
                
            </div>
            
            <a href="{{Route('home')}}"><input type="button" class="btn btn-primary" value="@lang("lang.back")"></button></a>
        </form>
    </div>
</div>
@endsection