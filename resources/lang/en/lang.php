<?php
return[
    "name"=>"Name",
    "email"=>"Email",
    "phone"=>"Phone",
    "profile"=>"Profile",
    "cdate"=>"Date Created",
    "udate"=>"Date Updated",
    "action"=>"Action",
    "brand"=>"Users Managment",
    "addUser"=>"Add Person",
    "view"=>"View",
    "edit"=>"Edit",
    "del"=>"Delete",
    "search"=>"Search",
    "back"=>"Back home",
    "add"=>"Add User",
    "fromdate"=>"From Date",
    "todate"=>"To Date",
    "update"=> "Update User",
    "null"=>"This field is required!",
    "login"=>"Login",
    "logout"=>"Logout"
    ];
?>