<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function getusers(Request $request){
        // $user=Auth::user();
        $user = $request->user();


        echo "Welcome ".$user->name;
    }
    function logout(){
        Auth::logout();  
        return redirect()->route("home");
    }

}
