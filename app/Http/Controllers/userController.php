<?php

namespace App\Http\Controllers;
use App\person;
use Illuminate\Http\Request;

class userController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    function index(){
        $persons=person::paginate(4);
       // $persons=person::All();
                   // ->paginate(6);
        // echo dd($persons);
         
         $arrFilter=['id','name','email','phone_number'];
         return view("index")->with("persons",$persons)
                             
                             ->with("text",'')
                             ->with("filter",'')
                             ->with("filters",$arrFilter)
                             ->with("dateFrom",'')
                             ->with("dateTo",'');
     }
    
    function create(){
        return view("addPerson");
    }
    function add(Request $request){
        //Option 1
        // $this->validate($request,[
        //     "name"=>"required",
        //     "email"=>"required",
        //     "phone"=>"required"
        // ]);
        //Option 2
        $request->validate([
            "name"=>"required|max:10|unique:persons",
            "email"=>"email",
            "phone_number"=>"digits_between:9,10"
        ]);

        $person=new person();
        $person->name=$request->name;
        $person->email=$request->email;
        $person->phone_number=$request->phone_number;
        $profile=$request->file("avator");
        
        $dest="public/image";

        $avatar=trim($profile->store($dest),"public");
        
        $person->profile=$avatar;
        $person->save();
       
         return redirect()->route('home');

    }
    function view($id){
        $person=person::where("id",$id)->first(); 
       // echo dd($person);
        return view("view")->with('person',$person);

    }

    function edit($id){
        $person=person::where("id",$id)->first(); 
       // echo dd($person);
        return view("edit")->with('person',$person);
    }

    function update($id,Request $request){

        $request->validate([
            "name"=>"required",
            "email"=>"email",
            "phone_number"=>"digits_between:9,10"
        ]);
        $person= person::find($id);
        $person->name = $request->name;
        $person->email= $request->email;
        $person->phone_number=$request->phone_number;
        $profile=$request->file("profile");
        
        $dest="public/image";

        $avatar=trim($profile->store($dest),"public");
        
        $person->profile=$avatar;
        //echo dd($person);
        $person->save();
        return redirect()->route("home");
 
     }

    function delete($id){
        //$article=Article::destroy($id);
         $person=person::where("id",$id)->delete();
        return redirect()->route('home');
    }

    function search( Request $request){
        
        $sch=$request->txtsearch;
        $filter=$request->filter;

        $dateFrom=$request->dFrom;
        $dateTo=$request->dTo;
        // echo dd($dateFrom);
        //echo dd($sch);
       
       
        $search=person::where($filter,"like","%".$sch."%")
                     //->wherebetween("created_at",[$dateFrom,$dateTo])
                       
                       // ->orwhere("author","like","%".$sch."%")

                     //   ->orwhere("id",$sch)
                        ->get();
         
                                    
        $arrFilter=['id','name','email','phone_number'];
        // echo dd($search);
        return view("index")->with("persons",$search)
                            
                            ->with("text",$sch)
                            ->with("filter",$filter)
                            ->with("filters",$arrFilter)
                            ->with("dateFrom",$dateFrom)
                            ->with("dateTo",$dateTo);

                           

                            
    }


    function language($lang){
        \App::setlocale($lang);
        \Session::put("lang",$lang);
        return redirect()->back();
    }
    function khmer(){
        \App::setlocale("kh");
        \Session::put("lang","kh");
        return redirect()->back();
    }
    function english(){
        \App::setlocale("en");
        \Session::put("lang","en");
        return redirect()->back();
    }
    
}
