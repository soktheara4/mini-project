<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');

// });
Route::get('/',"userController@index")->name("home");
route::get('/view/{id}',"userController@view")->name("view.person");
route::get('/delete/{id}',"userController@delete")->name("delete.person");
route::get('/create',"userController@create")->name("create.person")->middleware("auth.basic");
Route::post('/add',"userController@add")->name("add.person");

route::get('/edit/{id}',"userController@edit")->name("edit.person");
route::put('/update/{id}',"userController@update")->name("update.person");
Route::post("/search","userController@search")->name("seach.person");

route::get("/khmer","userController@khmer")->name("khmer");
route::get("/english","userController@english")->name("english");

route::get("/lang/{language}","userController@language"); 

//Authication
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


route::get('/users','HomeController@getusers');
route::get('/logout','HomeController@logout');

